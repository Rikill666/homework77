const path = require('path');

const express = require('express');
const multer = require('multer');
const nanoid = require('nanoid');

const fileDb = require('../fileDb');
const config = require('../config');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', async (req, res) => {
  const items = await fileDb.getItems();
  res.send(items);
});


router.post('/', upload.single('image'), async (req, res) => {
  const product = req.body;

  if (req.file) {
    product.image = req.file.filename;
  }
  if(req.body.title){
    await fileDb.addItem(product);
    res.send(req.body.id);
  }
  else{
    res.status(400).send({error: "Not written message"});
  }

});

module.exports = router;