import React from 'react';
import {Switch, Route} from "react-router-dom";

import Messages from "./containers/Messages/Messages";
import {Container} from "reactstrap";

const App = () => {
    return (
        <Container>
            <Switch>
                <Route path="/" exact component={Messages}/>
                <Route render={() => <h1>Not Found</h1>}/>
            </Switch>
        </Container>
    );
};

export default App;
