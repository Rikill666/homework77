import React from 'react';

import {apiURL} from "../../constants";

const styles = {
  width: '100px',
  height: '100px',
  marginRight: '10px'
};

const MessageThumbnail = props => {
  return props.image ?
      <img alt="message" src={apiURL + '/uploads/' + props.image} style={styles} className="img-thumbnail" />
      :null;


};

export default MessageThumbnail;