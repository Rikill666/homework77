import React, {Component, Fragment} from 'react';
import {Button, Card, CardBody, Col, Row} from "reactstrap";
import {connect} from "react-redux";

import MessageThumbnail from "../../components/MessageThumbnail/MessageThumbnail";
import {addNewMessage, getMessages} from "../../store/actions/messagesActions";
import MessageForm from "../../components/MessageForm/MessageForm";
import Spinner from "../../components/UI/Spinner/Spinner";
import Modal from "../../components/UI/Modal/Modal";

class Messages extends Component {
    state = {
        show: false
    };
    modalWindowShow = () => {
        this.setState({show: true});
    };
    modalWindowHide = () => {
        this.setState({show: false});
    };
    createNewMessage = (message) => {
        this.props.addNewMessage(message);
        this.modalWindowHide();
    };

    componentDidMount() {
        this.props.getMessages();
    }

    render() {
        return (
            <Fragment>
                <div style={{textAlign:"right"}}>
                    <Button style={{margin:"20px"}} onClick={this.modalWindowShow}>Add message</Button>
                </div>
                {this.props.loading ? <Spinner/> : this.props.messages.map(message => {
                    let authorName = message.author ? message.author : "Anonimus";
                    return <Card style={{marginBottom:"10px"}} key={message.id}>
                        <CardBody>
                            <Row>
                                <Col sm={6} md={4} lg={3} >
                                    <MessageThumbnail image={message.image}/>
                                    <strong style={{marginLeft: '10px', display:"block"}}>
                                        {authorName}
                                    </strong>
                                </Col>
                                <Col sm={6} md={8} lg={9}>
                                    <p>{message.title}</p>
                                </Col>
                            </Row>
                        </CardBody>
                    </Card>
                })}
                <Modal show={this.state.show} close={this.modalWindowHide}>
                    <MessageForm onSubmit={this.createNewMessage}/>
                </Modal>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    messages: state.messages.messages
});

const mapDispatchToProps = dispatch => ({
    getMessages: () => dispatch(getMessages()),
    addNewMessage: (message) => dispatch(addNewMessage(message)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Messages);
