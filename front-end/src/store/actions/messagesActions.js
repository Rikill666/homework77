import axiosApi from "../../axiosApi";

export const FETCH_MESSAGES_REQUEST = 'FETCH_MESSAGES_REQUEST';
export const FETCH_MESSAGES_SUCCESS = 'FETCH_MESSAGES_SUCCESS';
export const FETCH_MESSAGES_ERROR = 'FETCH_MESSAGES_ERROR';

export const CREATE_MESSAGE_REQUEST = 'CREATE_MESSAGE_REQUEST';
export const CREATE_MESSAGE_SUCCESS = 'CREATE_MESSAGE_SUCCESS';
export const CREATE_MESSAGE_ERROR= 'CREATE_MESSAGE_ERROR';

export const fetchMessagesRequest = () => {return {type: FETCH_MESSAGES_REQUEST};};
export const fetchMessagesSuccess = messages => ({type: FETCH_MESSAGES_SUCCESS, messages});
export const fetchMessagesError = (error) => {return {type: FETCH_MESSAGES_ERROR, error};};

export const createMessageRequest = () => {return {type: CREATE_MESSAGE_REQUEST};};
export const createMessageSuccess = () => ({type: CREATE_MESSAGE_SUCCESS});
export const createMessageError = (error) => {return {type: CREATE_MESSAGE_ERROR, error};};

export const getMessages = () => {
  return async dispatch => {
    try{
      dispatch(fetchMessagesRequest());
      const response = await axiosApi.get('/messages');
      const messages = response.data;
        dispatch(fetchMessagesSuccess(messages));
    }
    catch (e) {
      dispatch(fetchMessagesError(e));
    }
  };
};

export const addNewMessage = (message) => {
  return async dispatch => {
    try{
      dispatch(createMessageRequest());
      await axiosApi.post('/messages', message);
      dispatch(createMessageSuccess());
      dispatch(getMessages());
    }
    catch (e) {
      dispatch(createMessageError(e));
    }
  };
};