import {
  FETCH_MESSAGES_ERROR, FETCH_MESSAGES_REQUEST, FETCH_MESSAGES_SUCCESS,
} from "../actions/messagesActions";

const initialState = {
  messages: [],
  loading:false,
  error: null,
};

const messagesReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_MESSAGES_REQUEST:
      return {...state, loading: true};
    case FETCH_MESSAGES_SUCCESS:
      return {...state, messages: action.messages, error:null, loading: false};
    case FETCH_MESSAGES_ERROR:
      return {...state,error:action.error, loading: true};
    default:
      return state;
  }
};

export default messagesReducer;